from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from blog.apps.account.models import User
from blog.apps.post.models import Post

from . import serializers

class User(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer

class Post(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer
    permission_classes = (IsAuthenticated,)
