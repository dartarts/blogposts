"""
Helper functions for REST API.
"""

from django.conf import settings
import clearbit
import pyhunter

def check_whether_email_exists(email):
    """
    Perform checking whether email exist using service - Hunter.io.
    """
    hunter = pyhunter.PyHunter(settings.HUNTER_API_KEY)
    response = hunter.email_verifier(email)
    if response and response.get('sources'):
        return True
    return False

def request_user_data(email):
    """
    Request additional user data using service - clearbit.
    """
    clearbit.key = settings.CLEARBIT_KEY
    response = clearbit.Enrichment.find(email=email, stream=True)
    return response
