from rest_framework import serializers
from .utils import check_whether_email_exists, request_user_data
from blog.apps.account.models import User
from blog.apps.post.models import Post

class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'name', 'password')

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

class PostSerializer(serializers.HyperlinkedModelSerializer):

    username = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Post
        fields = ('url', 'title', 'text', 'like', 'unlike', 'username', 'author', 'created')

