from django.urls import path, re_path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from . import views

patterns = [
    path(r'users/', views.User.as_view(actions={'get': 'list',  'post' : 'create'}, suffix='List'), name='user-list'),
    path(r'user/<uuid:pk>', views.User.as_view(actions={'get': 'retrieve'}, suffix='Detail'), name='user-detail'),
    path(r'posts/', views.Post.as_view(actions={'get': 'list', 'post' : 'create'}, suffix='List'), name='post-list'),
    path(r'post/<uuid:pk>', views.Post.as_view(actions={'get': 'retrieve', 'put' : 'update'}, suffix='Detail'), name='post-detail'),
    path(r'token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path(r'token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
