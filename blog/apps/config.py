from django.apps import AppConfig

class account(AppConfig):
    name = 'blog.apps.account'

class post(AppConfig):
    name = 'blog.apps.post'

