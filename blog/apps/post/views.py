from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import CreateView, ListView, UpdateView
from .models import Post

class PostListView(ListView):
    model = Post
    template_name = 'post/post_list.html'
