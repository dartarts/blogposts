import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from blog.apps.account.models import User

class Post(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False, verbose_name=_('identifier'))
    title = models.CharField(max_length=100, verbose_name=_('title'))
    text = models.TextField(verbose_name=_('text'))
    like = models.PositiveIntegerField(default=0, verbose_name=_('like'))
    unlike = models.IntegerField(default=0, verbose_name=_('unlike'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('created'))
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts', verbose_name=_('author'))

    def __str__(self):
        return self.title
