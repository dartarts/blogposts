from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User

class UserForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = User
        fields = ('username', 'email')