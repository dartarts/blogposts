import uuid
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _

class User(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False, verbose_name=_('identifier'))
    username = models.CharField(max_length=150, unique=True, verbose_name=_('username'))
    name = models.CharField(max_length=30, blank=True, verbose_name=_('full name'))
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(default=False, verbose_name=_('staff status'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'name']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        return self.name

    def get_full_name(self):
        return self.name.strip()

    def get_short_name(self):
        return self.name.strip()
