from django.conf.urls import url
from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path(r'signup/', views.Signup.as_view(), name='signup'),
    path(r'login/', auth_views.LoginView.as_view(template_name='account/login.html'), name='login'),
    path(r'logout/', auth_views.LogoutView.as_view(), name='logout'),
]