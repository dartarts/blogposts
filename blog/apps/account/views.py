from django.shortcuts import render, redirect
from django.views.generic import CreateView, ListView, UpdateView
from django.urls import reverse_lazy
from django.contrib import messages
from blog.api.utils import check_whether_email_exists, request_user_data

from .forms import UserForm

class Signup(CreateView):

    form_class = UserForm
    success_url = reverse_lazy('login')
    template_name = 'account/signup.html'

    def form_valid(self, form):
        email_exist = check_whether_email_exists(form.cleaned_data.get('email'))
        user_data = request_user_data(form.cleaned_data.get('email'))
        if email_exist and user_data:
            user = form.save(commit=False)
            user.name = user_data['person']['name']['fullName']
            user.save()
            return redirect(self.success_url)
        else:
            messages.error(self.request, 'Hunter.io cant find your email! Please, verify your email!', extra_tags='alert')
        return super().form_invalid(form)
